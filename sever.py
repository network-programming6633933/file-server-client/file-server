import socket

filename = 'a.txt'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 12345))
s.listen()

print("Server is waiting for a connection...")

client_socket, client_address = s.accept()
print("Connected to", client_address)

with open(filename, 'rb') as file:
    data = file.read(1024)
    while data:
        client_socket.send(data)
        data = file.read(1024)

print("File sent to the client.")
client_socket.close()
s.close()
