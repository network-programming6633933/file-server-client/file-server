import socket


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('angsila.informatics.buu.ac.th', 12345))

buffer = b''

while True:
    data = s.recv(1024)
    if not data:
        break
    buffer += data

print(buffer.decode())
s.close()
